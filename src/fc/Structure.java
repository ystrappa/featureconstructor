/*
  This file is part of featureConstructor.

  featureConstructor is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  featureConstructor is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with featureConstructor.  If not, see <http://www.gnu.org/licenses/>.
*/

package fc;

import java.util.BitSet;
import java.util.Arrays;


public class Structure {
	private final BitSet[] _adjacencies;

	public Structure(int numberOfVariables) {
		this._adjacencies = new BitSet[numberOfVariables];

		for (int i = 0; i < numberOfVariables; i++)
			this._adjacencies[i] = new BitSet(numberOfVariables);
	}

	public Structure(Structure structure) {
		this._adjacencies = new BitSet[structure.getNumberOfVariables()];

		for (int i = 0; i < structure.getNumberOfVariables(); i++)
			this._adjacencies[i] = (BitSet) structure.getAdjacencies(i).clone();
	}

	public BitSet getAdjacencies(int variable) {
		return this._adjacencies[variable];
	}

	public void setAdjacencies(int variable, BitSet newAdj) {
		this._adjacencies[variable] = null;
		this._adjacencies[variable] = newAdj;
	}

	public void addEdge(int x, int y) {
		if (this._adjacencies[x] == null)
			this._adjacencies[x] = new BitSet(this.getNumberOfVariables());

		if (this._adjacencies[y] == null)
			this._adjacencies[y] = new BitSet(this.getNumberOfVariables());

		this._adjacencies[x].set(y, true);
		this._adjacencies[y].set(x, true);
	}

	public void rmEdge(int x, int y) {
		this._adjacencies[x].set(y, false);
		this._adjacencies[y].set(x, false);
	}

	public boolean getEdge(int x, int y) {
		return this._adjacencies[x].get(y);
	}

	public int getNumberOfVariables() {
		return _adjacencies.length;
	}

	public Structure clone() {
		return new Structure(this);
	}

	public boolean equals(Object object) {
		if (object instanceof Structure) {
			Structure st = (Structure) object;

			return Arrays.deepEquals(this._adjacencies, st._adjacencies);
		}
		else
			return false;
	}

	public int hashCode() {
		return Arrays.deepHashCode(this._adjacencies.clone());
	}

	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		int j;
		for (int i = 0; i < this.getNumberOfVariables(); i++) {
			for (j = 0; j < this.getNumberOfVariables() - 1; j++)
				stringBuilder.append((this._adjacencies[i].get(j) ? 1 : 0) + " ");

			stringBuilder.append((this._adjacencies[i].get(j) ? 1 : 0) + "\n");
		}

		return stringBuilder.toString();
	}
}
