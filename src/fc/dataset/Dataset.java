/*
  This file is part of featureConstructor.

  featureConstructor is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  featureConstructor is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with featureConstructor.  If not, see <http://www.gnu.org/licenses/>.
*/

package fc.dataset;

import fc.feature.Feature;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.BitSet;

public class Dataset {
	private final List<Feature> _list;
	private final Map<Feature, Integer> _map;
	private int _numberOfVariables;
	private int _numberOfExamples;
	private List<Integer> _card;


	public Dataset(int maximumNumberOfFeatures) {
		this._list = new ArrayList<Feature>(maximumNumberOfFeatures);
		this._card = new ArrayList<Integer>();
		this._numberOfVariables = 0;
		this._map = new HashMap<Feature, Integer>();
	}

	// only add unique examples
	public void addExample(Feature example) {
		Integer value = this._map.get(example);

		if (value == null) {
			this._map.put(example, 1);
			this.getList().add(example);
		} else {
			this._map.put(example, value + 1);
		}
	}

	public int getCount(Feature example) {
		Integer count = this._map.get(example);
		return (count == null) ? 0 : count;
	}

	public int getCountFromIndex(int index) {
		return this.getCount(this._list.get(index));
	}

	public List<Feature> getUniqueFeatures() {
		java.util.Set<Feature> setOfFeatures = this._map.keySet();
		List<Feature> uniqueFeatures = new ArrayList<Feature>(setOfFeatures.size());

		for (Feature feature : setOfFeatures) uniqueFeatures.add(feature.clone());

		return uniqueFeatures;
	}

	public Feature getExample(int index) {
		return this.getList().get(index).clone();
	}

	public List<Feature> getElements() {
		return Collections.unmodifiableList(this.getList());
	}

	public List<Feature> getList() {
		return this._list;
	}

	public int getNumberOfVariables() {
                return this._numberOfVariables;
        }

        public void setNumberOfVariables(int numberOfVariables) {
                this._numberOfVariables = numberOfVariables;
        }

	public void setCard(List<Integer> card) {
		this._card = card;
	}

	public int getCard(int var) {
		return this._card.get(var);
	}

	public List<Integer> getCard() {
		return Collections.unmodifiableList(this._card);
	}

	public Map<Feature, Integer> getMap() {
		return _map;
	}
}