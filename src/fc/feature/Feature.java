/*
  This file is part of featureConstructor.

  featureConstructor is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  featureConstructor is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with featureConstructor.  If not, see <http://www.gnu.org/licenses/>.
*/

package fc.feature;

import java.util.Arrays;
import java.util.BitSet;


/**
 * This abstract class represents a feature, which is a conjunction of variable
 * assignments.
 */
public abstract class Feature {

    private final BitSet _var;
    private final int _numberOfDomainVariables;

    public Feature(int n) {
        this._numberOfDomainVariables = n;
        this._var = new BitSet(n);
    }

    protected Feature(int n, BitSet var) {
        this._numberOfDomainVariables = n;
        this._var = (BitSet) var.clone();
    }

    public BitSet getVar() {
        return this._var;
    }

    public void addVar(int var) {
        this._var.set(var);
    }

    public void rmVar(int var) {
        this._var.clear(var);
    }

    protected int getNumberOfDomainVariables() {
        return this._numberOfDomainVariables;
    }

    public abstract boolean isSatisfied(Feature otherFeature);

    public abstract void addVal(int var, Integer val);

    public abstract Integer getVal(int var);

    public abstract int getLength();

    @Override
    public abstract Feature clone();

    @Override
    public abstract boolean equals(Object object);

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this._var == null ? 0 : this._var.hashCode());

        return hash;
    }

    public abstract String toString();
}
