/*
  This file is part of featureConstructor.

  featureConstructor is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  featureConstructor is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with featureConstructor.  If not, see <http://www.gnu.org/licenses/>.
*/

package fc.util;

import java.util.Set;
import java.util.BitSet;
import java.util.HashSet;
import java.util.List;
import java.util.Collection;

import fc.feature.*;
import fc.Structure;


public class Util {
	// /**
	//  * Finding maximal clique in graph by using Bron-Kerbosh Algorithm version 1.
	//  *
	//  * @param clique     Set whose elements are maximal cliques in the graph
	//  * @param compsub    Potential clique between calls
	//  * @param candidates Candidates to be part of a clique
	//  * @param not        Nodes not used between calls
	//  *
	//  * @todo improve selection to pivot by selectioning a node with many
	//  * neighborshood. The improve in the algorithm become it to version 2.
	//  */
	public static void
	bronKerboschAlgorithm(Collection<BitSet> cliques,
			      BitSet compsub,
			      BitSet candidates,
			      BitSet not,
			      Structure neighbors) {
		// Is found a clique?
		if (candidates.cardinality() == 0 && not.cardinality() == 0) {
			cliques.add(compsub);
			return;
		}

		BitSet nodesToSelect = (BitSet) candidates.clone();
		nodesToSelect.or(not);
		int pivot = nodesToSelect.nextSetBit(0);
		BitSet neighborsOfPivot = neighbors.getAdjacencies(pivot);
		BitSet inspect = (BitSet) candidates.clone();

		for (int i = neighborsOfPivot.nextSetBit(0); i >= 0; i = neighborsOfPivot.nextSetBit(i+1))
			inspect.set(i, false); //removeAll(neighborsOfPivot);

		BitSet newCompsub;
		BitSet neighborsOfSelected;
		BitSet newCandidates;
		BitSet newNot;
		for (int selectedNode = inspect.nextSetBit(0);
		     selectedNode >= 0;
		     selectedNode = inspect.nextSetBit(selectedNode+1)) {

			newCompsub = (BitSet) compsub.clone();

			newCompsub.set(selectedNode, true);

			neighborsOfSelected = neighbors.getAdjacencies(selectedNode);

			newCandidates = (BitSet) candidates.clone();

			// newCandidates.retainAll(neighborsOfSelected);
			newCandidates.and(neighborsOfSelected);

			newNot = (BitSet) not.clone();

			// newNot.retainAll(neighborsOfSelected);
			newNot.and(neighborsOfSelected);

			bronKerboschAlgorithm(cliques,
					      newCompsub,
					      newCandidates,
					      newNot,
					      neighbors);

			candidates.set(selectedNode, false);
			not.set(selectedNode, true);
		}

	}

	public static Collection<BitSet> factorizeToCompleteSubgraphs(Structure neighbors)
	{
		Collection<BitSet> cliques = new java.util.HashSet<BitSet>();

		BitSet initialCandidates = new BitSet(neighbors.getNumberOfVariables());
		initialCandidates.set(0, neighbors.getNumberOfVariables(), true);
		bronKerboschAlgorithm(cliques,
				      new BitSet(neighbors.getNumberOfVariables()),
				      initialCandidates,
				      new BitSet(neighbors.getNumberOfVariables()),
				      neighbors);

		return cliques;
	}
}
