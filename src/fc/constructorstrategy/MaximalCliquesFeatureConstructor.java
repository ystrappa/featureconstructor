/*
  This file is part of featureConstructor.

  featureConstructor is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  featureConstructor is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with featureConstructor.  If not, see <http://www.gnu.org/licenses/>.
*/

package fc.constructorstrategy;

import fc.Structure;
import fc.dataset.Dataset;
import fc.util.Util;
import fc.util.FileHandler;
import fc.settings.Settings;

import java.util.BitSet;
import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * MaximalCliqueFeatureConstructor makes features based on the structure's
 * maximal cliques.
 * </p><p>
 * First, it finds the maximal cliques for each graph in the structure. Then,
 * for each context associated with that graph, it makes a feature whose
 * variables are the variables of the clique and its values are the values in
 * the context.
 * </p>
 *
 */
public class MaximalCliquesFeatureConstructor extends FeatureConstructorStrategy {
	protected int _numberOfMaximumCliques;
	protected int _maximumSizeOfMaximumCliques;

	public MaximalCliquesFeatureConstructor(Settings settings) {
		super(settings);

		this._numberOfMaximumCliques = 0;
		this._maximumSizeOfMaximumCliques = 0;
	}

	@Override
	public void constructFeatures(Dataset dataset,
				      Map<Structure, List<Integer>> structures) {
		Collection<BitSet> cliques;
		for (Structure structure : structures.keySet()) {
			// get maximum cliques from structure
			cliques = Util.factorizeToCompleteSubgraphs(structure);

			// create a feature for each maximum clique
			for (BitSet clique : cliques) {
				this._numberOfMaximumCliques++;

				if (clique.cardinality() > this._maximumSizeOfMaximumCliques)
					this._maximumSizeOfMaximumCliques =
						clique.cardinality();

				for (Integer idxCtx : structures.get(structure))
					addFeature(clique, idxCtx);
			}
		}


		FileHandler.writeMessage(this._settings.getLogFile(),
					 "Number of maximum cliques: " +
					 this._numberOfMaximumCliques);

		FileHandler.writeMessage(this._settings.getLogFile(),
					 "Maximum size of maximum cliques: " +
					 this._maximumSizeOfMaximumCliques);
	}
}
