/*
  This file is part of featureConstructor.

  featureConstructor is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  featureConstructor is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with featureConstructor.  If not, see <http://www.gnu.org/licenses/>.
*/

package fc;

import fc.constructorstrategy.FeatureConstructorFactory;
import fc.constructorstrategy.FeatureConstructorStrategy;
import fc.settings.Settings;
import fc.settings.FeatureType;
import fc.feature.Feature;
import fc.util.FileHandler;
import fc.dataset.Dataset;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.BitSet;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Generates and writes a set of features to a features file (.features), from a
 * structures file (.net) that defines a set of Markov Networks for different
 * contexts.
 *
 * It reads a settings file with the following parameters:
 *
 * network path (containing .net files)
 * features path (for storing the output .features files)
 * log path
 * feature type (binary or positive)
 * feature construction strategy (see FeatureConstructorFactory)
 * threshold 1 (generation step) (for resampling)
 * threshold 2 (selection step) (for resampling)
 * seed for random number generator
 *
 *
 * @author Yanela Strappa
 */
public class FeatureConstructor {
	public static void main(String[] args) {
		long startTime;
		long totalTime;

		if (args.length < 3) {
			System.out.println("Please provide " +
					   "a configuration file, " +
					   "a (wdata) dataset, " +
					   "a network name.");
			System.exit(1);
		}

		try {
			// Read settings and network file
			Settings conf = new Settings(args[0],
						     args[1],
						     args[2]);

			Runtime runtime = Runtime.getRuntime();

			// Read dataset from file
			Dataset dataset = FileHandler.readDataset(conf.getDatasetFile(), 0);
			int numberOfVariables = dataset.getNumberOfVariables();

			double numberOfContexts = dataset.getMap().size();

			// read networks from input file
			Map<Structure, List<Integer>> structures =
				FileHandler.readStructure(conf.getNetworkFile(),
							  numberOfVariables);

			FileHandler.writeMessage(conf.getLogFile(),
						 "Number of structures: " +
						 structures.size() + "/" +
						 dataset.getMap().size());

			constructAdjacencyMatrix(structures,
						 dataset.getNumberOfVariables(),
						 numberOfContexts,
						 conf);


			// generate features from structures
			FeatureConstructorStrategy fc =
				FeatureConstructorFactory.getInstance().
				getFeatureConstructor(conf);

			startTime = System.currentTimeMillis();

			Map<BitSet, List<Integer>> setOfFeatures =
				fc.generateFeatures(dataset, structures);

			// call garbage collector
			structures = null;
			runtime.gc();

			totalTime = System.currentTimeMillis() - startTime;

			Map<String, Integer> fff = constructStringFeatures(dataset,
									   setOfFeatures,
									   conf);
			// call garbage collector
			setOfFeatures = null;
			runtime.gc();

			// write features to an output file
			FileHandler.writeFeatures(conf.getFeatureFile(),
						  dataset.getNumberOfVariables(),
						  fff.keySet());

			FileHandler.writeMessage(conf.getLogFile(),
						 "Total time for constructing features: " +
						 totalTime + " ms");

			FileHandler.writeMessage(conf.getLogFile(),
						 "Number of blankets: " +
						 BitSetFactory._setOfBlankets.size() + "/" +
						 (numberOfContexts *
						  dataset.getNumberOfVariables()));

			FileHandler.writeMessage(conf.getLogFile(),
						 "Number of unique features: " +
						 fff.size());

			FileHandler.writeMessage(conf.getLogFile(),
						 "Used memory is: " +
						 (runtime.totalMemory() - runtime.freeMemory()) +
						 " bytes");
		} catch (FileNotFoundException e) {
			System.err.println("The configuration file was not found.");
			System.exit(1);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}

		System.exit(0);
	}

	private static Map<String, Integer>
		constructStringFeatures(Dataset dataset,
					Map<BitSet,List<Integer>> features,
					Settings settings) {
		FeatureType featureType = settings.getFeatureType();

		// Features along as their frequencies
		Map<String, Integer> featureFrequency = new HashMap<String, Integer>();

		for (Map.Entry<BitSet, List<Integer>> obj : features.entrySet()) {
			BitSet f = obj.getKey();

//			System.out.println(f + " " + obj.getValue().size());

			// construct a feature for each context
			for (Integer idContext : obj.getValue()) {
				Feature context = dataset.getList().get(idContext);

				// construct feature as a string
				StringBuilder stringBuilder = new StringBuilder();

				for (int i = f.nextSetBit(0); i >= 0; i = f.nextSetBit(i + 1)) {
					// print positive or mixed feature
					// according to settings
					if (featureType == FeatureType.POSITIVE &&
					    context.getVal(i) < 1)
						continue;

					stringBuilder.append("+v" +
							     i +
							     "_" +
							     context.getVal(i) + " ");
				}

				// obtain feature
				String out = stringBuilder.toString().trim();

				// add new features to the set of features
				Integer freq = featureFrequency.get(out);

				// if feature exists in the set, then increase its counter
				featureFrequency.put(out, (freq == null) ? 1 : freq + 1);
			}
		}

		// add unitary features
		for (int i = 0; i < dataset.getNumberOfVariables(); i++)
			for (int v = 0; v < dataset.getCard(i); v++) {
				if (featureType == FeatureType.POSITIVE &&
				    v < 1)
					continue;
				String out2 = "+v" + i + "_" + v;
				Integer value = featureFrequency.get(out2);
				featureFrequency.put(out2, (value == null) ? 1 : value + 1);
			}


		return featureFrequency;
	}

	private static void
	constructAdjacencyMatrix(Map<Structure, List<Integer>> structures,
				 int numberOfVariables,
				 double numberOfContexts,
				 Settings conf) {
		int[][] adjMatrix = new int[numberOfVariables][numberOfVariables];

		int count = 0;
		for (Map.Entry<Structure, List<Integer>> obj : structures.entrySet()) {
			Structure s = obj.getKey();

			for (int i = 0; i < numberOfVariables; i++)
				for (int j = i+1; j < numberOfVariables; j++) {
					count = s.getEdge(i, j) ?
						obj.getValue().size() :
						-obj.getValue().size();

					adjMatrix[i][j] += count;
					adjMatrix[j][i] += count;
				}
		}

		StringBuilder stringBuilder;
		for (int i = 0; i < numberOfVariables; i++) {
			stringBuilder = new StringBuilder();

			for (int j = 0; j < numberOfVariables; j++)
				stringBuilder.append(adjMatrix[i][j] / numberOfContexts + " ");

			FileHandler.writeMessage(conf.getAdjMatrixFile(),
						 stringBuilder.toString().trim(),
						 false);
		}
	}

	public static class BitSetFactory {
		public static HashMap<BitSet, BitSet> _setOfBlankets =
			new HashMap<BitSet, BitSet>();

		public static BitSet lookup(BitSet bs) {
			BitSet lookedBs = null;

			if (!BitSetFactory._setOfBlankets.containsKey(bs)) {
				BitSetFactory._setOfBlankets.put(bs, bs);
				lookedBs = bs;
			} else
				lookedBs = BitSetFactory._setOfBlankets.get(bs);

			return lookedBs;
		}
	}
}
