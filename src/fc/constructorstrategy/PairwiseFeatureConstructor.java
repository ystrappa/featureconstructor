/*
  This file is part of featureConstructor.

  featureConstructor is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  featureConstructor is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with featureConstructor.  If not, see <http://www.gnu.org/licenses/>.
*/

package fc.constructorstrategy;

import fc.Structure;
import fc.dataset.Dataset;
import fc.feature.BinaryFeature;
import fc.feature.Feature;
import fc.util.FileHandler;
import fc.settings.Settings;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * PairwiseFeatureConstructor
 *
 * Generates one feature for each pair of adjacent variables, for every
 * contextual graph.
 *
 *
 * @author Yanela Strappa and ae
 */
public class PairwiseFeatureConstructor extends FeatureConstructorStrategy {

	public PairwiseFeatureConstructor(Settings settings) {
		super(settings);
	}

	@Override
	public void constructFeatures(Dataset dataset,
				      Map<Structure, List<Integer>> structures) {
		int numberOfVariables = dataset.getNumberOfVariables();

		BitSet adj;
		BitSet pair = new BitSet(numberOfVariables);
		for (Structure graph : structures.keySet()) {
			pair.clear();

			for (int x = 0; x < numberOfVariables; x++) {
				adj = graph.getAdjacencies(x);
				pair.set(x, true);

				for (int a = adj.nextSetBit(0);
				     a >= 0;
				     a = adj.nextSetBit(a + 1)) {
					pair.set(a, true);

					for (Integer idxCtx : structures.get(graph))
						addFeature((BitSet)pair.clone(), idxCtx);

					pair.set(a, false);
				}

				pair.set(x, false);
			}
		}
	}
}
