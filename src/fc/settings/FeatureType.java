package fc.settings;

public enum FeatureType {
	POSITIVE("positive"),
	MIXED("mixed"),
	BINARY("binary");

	private final String _description;

	private FeatureType(String description) {
		this._description = description;
	}

	public String toString() {
		return this._description;
	}
}