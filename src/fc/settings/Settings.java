package fc.settings;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Yanela Strappa
 */
public class Settings {
	private final String _networkName;
	private final String _datasetName;
	private final String _datasetDirectory;
	private final String _logDirectory;
	private final String _outDirectory;
	private final String _outputFile;
	private final FeatureType _featureType;
	private final String _featureConstructionType;

	// Read the configuration
	public Settings(String pathToSettingsFile,
			String datasetName,
			String networkName)
		throws FileNotFoundException, IOException {
		this._datasetName = datasetName;
		this._networkName = networkName;
		Scanner scanner = new Scanner(new File(pathToSettingsFile));
		this._datasetDirectory = scanner.nextLine();
		this._logDirectory = scanner.nextLine();
		this._outDirectory = scanner.nextLine();

		// Set type of feature
		String strFeatureType = scanner.nextLine();

		if (strFeatureType.equalsIgnoreCase("positive")) {
			this._featureType = FeatureType.POSITIVE;
		} else if (strFeatureType.equalsIgnoreCase("mixed")) {
			this._featureType = FeatureType.MIXED;
		} else {
			this._featureType = FeatureType.BINARY;
		}

		this._featureConstructionType = scanner.nextLine();

		// create name of output file
		this._outputFile = createOutputFileName();
	}

	public Settings(String datasetName,
			String datasetDirectory,
			String networkName,
			String logDirectory,
			String outDirectory,
			FeatureType featureType,
			String featureConstructionType) {
		this._datasetName = datasetName;
		this._datasetDirectory = datasetDirectory;
		this._networkName = networkName;
		this._logDirectory = logDirectory;
		this._outDirectory = outDirectory;
		this._featureType = featureType;
		this._featureConstructionType = featureConstructionType;
		this._outputFile = createOutputFileName();
	}

	private String createOutputFileName() {
		return new File(this._networkName).getName() + "." +
			this._featureType + "." +
			this._featureConstructionType + "." +
			"mn";
	}

	public File getDatasetFile() {
		return new File(this.getDatasetDirectory()
				+ File.separator
				+ this.getDatasetName());
	}

	public File getNetworkFile() {
		return new File(this.getNetworkName());
	}

	public File getLogFile() {
		return new File(this.getLogDirectory() +
				File.separator +
				getFeatureFile().getName() +
				".log");
	}

	public File getAdjMatrixFile() {
		return new File(this.getLogDirectory() +
				File.separator +
				getFeatureFile().getName() +
				".data");
	}

	public File getFeatureFile() {
		return new File(this.getOutDirectory() +
				File.separator +
				this._outputFile);
	}

	private String getLogDirectory() {
		return this._logDirectory;
	}

	private String getOutDirectory() {
		return this._outDirectory;
	}

	public FeatureType getFeatureType() {
		return this._featureType;
	}

	public String getFeatureConstructionType() {
		return _featureConstructionType;
	}

	public String getNetworkName() {
		return _networkName;
	}

	private String getDatasetDirectory() {
		return this._datasetDirectory;
	}

	private String getDatasetName() {
		return this._datasetName;
	}
}
