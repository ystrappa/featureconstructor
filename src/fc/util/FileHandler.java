/*
  This file is part of featureConstructor.

  featureConstructor is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  featureConstructor is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with featureConstructor.  If not, see <http://www.gnu.org/licenses/>.
*/

package fc.util;

import fc.feature.*;
import fc.Structure;
import fc.dataset.Dataset;
import fc.settings.Settings;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;

import java.util.Collection;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import java.text.SimpleDateFormat;


public class FileHandler {
	private static void
	readWDataDataset(File inputFile, Dataset dataset)
		throws IOException {
		// Open the file reader
		FileReader fileReader = new FileReader(inputFile);
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		int numberOfVariables = 0;
		String line = null;

		while ((line = bufferedReader.readLine()) != null) {
			String[] parts = line.trim().split("\\|");
			String[] items = parts[1].split(",");

			numberOfVariables = items.length;
			Feature example = new BinaryFeature(numberOfVariables);

			for (int i = 0; i < numberOfVariables; i++) {
				example.addVal(i, Integer.parseInt(items[i]));
			}

			for (int i = 0; i < Integer.parseInt(parts[0]); i++) {
				dataset.addExample(example);
			}
		}

		bufferedReader.close();

		// Set cardinalities
		dataset.setNumberOfVariables(numberOfVariables);
		List<Integer> card = new ArrayList<Integer>();

		for (int i = 0; i < dataset.getNumberOfVariables(); i++) card.add(2);

		dataset.setCard(card);
	}

	public static Dataset
	readDataset(File inputFile, int maximumNumberOfFeatures) {
		Dataset dataset = new Dataset(maximumNumberOfFeatures);

		try {
			readWDataDataset(inputFile, dataset);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		return dataset;
	}

	/**
	 * Reads a .net file containing a series of graphs and contexts lists
	 * associated to those graphs.
	 *
	 * @param networkFile the .net file to read from
	 *
	 */
	public static Map<Structure, List<Integer>>
		readStructure(File networkFile,
			      int numberOfVariables) {

		Map<Structure, List<Integer>> structures = new HashMap<Structure, List<Integer>>();

		try {
			FileReader fileReader = new FileReader(networkFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			String line;
			Structure structure = null;
			List<Integer> contexts = null;
			String[] strChunks = null;
			int i;
			while (bufferedReader.ready()) {
				line = bufferedReader.readLine();

				strChunks = line.split(";");

				structure = new Structure(numberOfVariables);

				// this variable indexes the ith blanket
				i = 0;
				for (String strBlanket : strChunks[0].split(",", -1)) {
					for (String strVar : strBlanket.split(" ", -1))
						if (strVar.length() != 0)
							structure.addEdge(i, Integer.parseInt(strVar));
					i++;
				}

				contexts = structures.containsKey(structure) ?
					structures.get(structure) : new ArrayList<Integer>();

				for (String strIdxCtx : strChunks[1].split(" ", -1))
					contexts.add(Integer.parseInt(strIdxCtx));

				for (int var = 0; var < numberOfVariables; var++)
					structure.setAdjacencies(var,
								 fc.FeatureConstructor.
								 BitSetFactory.
								 lookup(structure.getAdjacencies(var)));

				// Add an entry (structure,contexts) to the map
				structures.put(structure, contexts);
			}

			// Close the writer
			bufferedReader.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}

		return structures;
	}

	public static void
	writeFeatures(File featureFile, int numberOfVariables, Collection<String> features) {


		try {
			// FileWriter fileWriter = new FileWriter(settings.getFeatureFile(), true);
			FileWriter fileWriter = new FileWriter(featureFile);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			// write header (this assumes binary variables)
			for (int i = 0; i < numberOfVariables - 1; i++)
				bufferedWriter.write("2,");

			bufferedWriter.write("2\nMN {\n");

			// write body
			for (String f : features)
				bufferedWriter.write("0.000 " + f + "\n");

			// write footer
			bufferedWriter.write("}");

			bufferedWriter.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}

	public static void writeMessage(File logFile, String message) {
		writeMessage(logFile, message, true);
	}

	public static void writeMessage(File logFile, String message, boolean flag) {
		try {
			// Open the writer
			FileWriter fileWriter = new FileWriter(logFile, true);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

			// Write the message
			if (flag)
				bufferedWriter.write(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").
						     format(new Date()) + "\t");
			bufferedWriter.write(message + "\n");

			// Close the writer
			bufferedWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
