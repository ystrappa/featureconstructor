/*
  This file is part of featureConstructor.

  featureConstructor is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  featureConstructor is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with featureConstructor.  If not, see <http://www.gnu.org/licenses/>.
*/

package fc.constructorstrategy;

import fc.settings.Settings;

/**
 *
 * @author yane and ae
 */
public class FeatureConstructorFactory {
	protected static final FeatureConstructorFactory _instance =
		new FeatureConstructorFactory();

	public static FeatureConstructorFactory getInstance() {
		return _instance;
	}

	public FeatureConstructorStrategy getFeatureConstructor(Settings settings)
		throws IllegalArgumentException {

		String strategy = settings.getFeatureConstructionType();

		FeatureConstructorStrategy fcs = null;

		if (strategy.equals("mb"))
			fcs = new MarkovBlanketsFeatureConstructor(settings);
		else if (strategy.equals("pairwise"))
			fcs = new PairwiseFeatureConstructor(settings);
		else if (strategy.equals("cliques"))
			fcs = new MaximalCliquesFeatureConstructor(settings);
		else if (strategy.equals("powerset"))
			fcs = new PowerSetFeatureConstructor(settings);
		else //default
			throw new IllegalArgumentException("Invalid Feature Constructor type.");

		return fcs;
	}
}
