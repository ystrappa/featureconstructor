/*
  This file is part of featureConstructor.

  featureConstructor is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  featureConstructor is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with featureConstructor.  If not, see <http://www.gnu.org/licenses/>.
*/

package fc.constructorstrategy;

import fc.Structure;
import fc.dataset.Dataset;
import fc.feature.BinaryFeature;
import fc.feature.Feature;
import fc.settings.Settings;
import fc.util.PowerSet;
import fc.util.Util;

import java.util.BitSet;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * PowerSetFeatureConstructor Makes features based on the power sets of maximal
 * cliques in the structure.
 * </p>
 * <p>
 * First, it finds the maximal cliques for each graph in the structure. Then, it
 * iterates over the power set of each clique. For each context associated with
 * that graph, it makes a feature whose variables are the variables of the
 * element (of the power set of the clique) and its values are the values in the
 * context.
 * </p>
 *
 * Pseudocode
 *
 * <pre>
 *
 * D <- \empty
 *
 * for each graph g in M
 *   L <- M(G)
 *   for each mc_i in G
 *     for each p in PowerSet(mc_i)
 *       for each context c_i in L
 *         Let f  be a feature constructed by contextualizing p on c_i
 *         D <- D \\union f
 *       end for
 *     end for
 *   end for
 * end for
 *
 * </pre> @author Yanela Strappa and ae
 */
public class PowerSetFeatureConstructor extends FeatureConstructorStrategy {

	public PowerSetFeatureConstructor(Settings settings) {
		super(settings);
	}

	@Override
	public void constructFeatures(Dataset dataset,
				      Map<Structure, List<Integer>> structures) {

		Collection<BitSet> cliques;
		for (Structure graph : structures.keySet()) {

			cliques = Util.factorizeToCompleteSubgraphs(graph);

			for (BitSet c : cliques) {
				for (int i = 1; i < c.length(); i++) {
					PowerSet ps = new PowerSet(c, i);
					for (BitSet bs : ps) {
						for (Integer idxCtx : structures.get(graph))
							addFeature(bs, idxCtx);
					}
				}
			}
		}
	}
}
