/*
  This file is part of featureConstructor.

  featureConstructor is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  featureConstructor is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with featureConstructor.  If not, see <http://www.gnu.org/licenses/>.
*/

package fc.constructorstrategy;

import fc.Structure;
import fc.dataset.Dataset;
import fc.feature.BinaryFeature;
import fc.feature.Feature;
import fc.settings.Settings;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * SimpleFeatureConstructor makes a feature comprising the variables x and its
 * blanket for a given context.
 *
 *
 * @author yane and ae
 */
public class MarkovBlanketsFeatureConstructor extends FeatureConstructorStrategy {

	public MarkovBlanketsFeatureConstructor(Settings settings) {
		super(settings);
	}

	@Override
	public void constructFeatures(Dataset dataset,
				      Map<Structure, List<Integer>> structures) {
		int numberOfVariables = dataset.getNumberOfVariables();

		for (Structure structure : structures.keySet()) {
			// for each node in domain
			for (int x = 0; x < numberOfVariables; x++) {
				BitSet vars = (BitSet) structure.getAdjacencies(x).clone();
				vars.set(x, true);

				// construct feature for each context associated to structure
				for (Integer idxCtx : structures.get(structure))
					addFeature(vars, idxCtx);
			}
		}
	}
}
